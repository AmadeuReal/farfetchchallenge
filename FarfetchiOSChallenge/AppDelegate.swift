//
//  AppDelegate.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = UINavigationController(rootViewController: HeroListingController())
    window?.makeKeyAndVisible()
    return true
  }
}
