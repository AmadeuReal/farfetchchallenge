//
//  Heroswift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit
class HeroAvatar: FetchableImage, Circle {
  override func layoutSubviews() {
    setForm()
  }
}
class HeroCell: UICollectionViewCell, ReusableView {

  lazy var avatar: HeroAvatar = HeroAvatar(session: URLSession.shared)
  lazy var label: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = 0
    label.font = .boldSystemFont(ofSize: 14)
    return label
  }()

  lazy var detailLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = .systemFont(ofSize: 11)
    label.numberOfLines = 3
    label.textColor = .lightGray
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    backgroundColor = .white

    avatar.layer.borderWidth = 1
    avatar.layer.borderColor = UIColor.white.cgColor

    avatar.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(avatar)
    contentView.addSubview(label)
    contentView.addSubview(detailLabel)

    let margin = contentView.layoutMarginsGuide
    NSLayoutConstraint.activate([

      avatar.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
      avatar.widthAnchor.constraint(equalTo: avatar.heightAnchor),
      avatar.heightAnchor.constraint(equalTo: margin.heightAnchor, multiplier: 0.75),
      avatar.centerYAnchor.constraint(equalTo: margin.centerYAnchor),

      label.leadingAnchor.constraint(equalTo: avatar.trailingAnchor, constant: 8),
      label.trailingAnchor.constraint(lessThanOrEqualTo: margin.trailingAnchor),
      label.topAnchor.constraint(equalTo: margin.topAnchor),
      label.heightAnchor.constraint(equalToConstant: 18),

      detailLabel.leadingAnchor.constraint(equalTo: label.leadingAnchor),
      detailLabel.topAnchor.constraint(equalTo: label.bottomAnchor),
      detailLabel.bottomAnchor.constraint(equalTo: margin.bottomAnchor, constant: 8),
      detailLabel.trailingAnchor.constraint(lessThanOrEqualTo: margin.trailingAnchor)
      ])

    contentView.layer.cornerRadius = 4.0
    contentView.layer.borderWidth = 1.0
    contentView.layer.borderColor = UIColor.clear.cgColor
    contentView.layer.masksToBounds = false
    layer.shadowColor = UIColor.gray.cgColor
    layer.shadowOffset = .zero
    layer.shadowRadius = 4.0
    layer.shadowOpacity = 1.0
    layer.masksToBounds = false
    layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath

  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  func setup(with hero: SuperHero) {
    avatar.currentUrl = hero.thumbnail?.fullPath
    label.text = hero.name
    detailLabel.text = hero.description
    label.sizeToFit()
    detailLabel.sizeToFit()
  }
}
