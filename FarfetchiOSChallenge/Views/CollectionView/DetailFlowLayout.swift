//
//  DetailFlowLayout.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class DetailFlowLayout: UICollectionViewFlowLayout {

  private var firstSetupDone = false

  override func prepare() {
    super.prepare()
    if !firstSetupDone {
      setup()
      firstSetupDone = true
    }
  }

  private func setup() {
    guard let collectionView = collectionView else {
      return
    }
    headerReferenceSize = CGSize(width: collectionView.bounds.width, height: 20)
    scrollDirection = .vertical
    minimumLineSpacing = 3
    itemSize = CGSize(width: collectionView.bounds.width, height: 30)
    collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
  }
}
