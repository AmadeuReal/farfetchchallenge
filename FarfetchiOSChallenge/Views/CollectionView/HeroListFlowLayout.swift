//
//  SnappingFlowLayout.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class HeroListFlowLayout: UICollectionViewFlowLayout {

  private var firstSetupDone = false

  override func prepare() {
    super.prepare()
    if !firstSetupDone {
      setup()
      firstSetupDone = true
    }
  }

  private func setup() {
    guard let collectionView = collectionView else {
      return
    }
    scrollDirection = .vertical
    minimumLineSpacing = 20
    itemSize = CGSize(width: collectionView.bounds.width, height: 75)
    collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
  }
}
