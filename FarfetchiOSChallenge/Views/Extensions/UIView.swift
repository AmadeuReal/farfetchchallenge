//
//  UIView.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit.UIView

protocol Circle {
  func setForm()
}

extension UIView {
  func pin(into view: UIView) {
    translatesAutoresizingMaskIntoConstraints = false
    let margin = view.layoutMarginsGuide
    NSLayoutConstraint.activate([
      leadingAnchor.constraint(equalTo: margin.leadingAnchor),
      trailingAnchor.constraint(equalTo: margin.trailingAnchor),
      topAnchor.constraint(equalTo: margin.topAnchor),
      bottomAnchor.constraint(equalTo: margin.bottomAnchor)
      ])
  }
}

extension Circle where Self: UIView {
  func setForm() {
    layer.cornerRadius = frame.height * 0.5
    clipsToBounds = true
  }
}
