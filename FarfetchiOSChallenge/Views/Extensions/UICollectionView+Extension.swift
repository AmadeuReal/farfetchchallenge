//
//  UICollectionView+Extension.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit.UICollectionView

extension UICollectionView {
  func register<T: UICollectionViewCell> (_: T.Type) where T: ReusableView {
    register(T.self, forCellWithReuseIdentifier: T.identifier)
  }
  func dequeueCell<T: ReusableView>(class: T.Type, for indexPath: IndexPath) -> T? {
    return dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T
  }
}
