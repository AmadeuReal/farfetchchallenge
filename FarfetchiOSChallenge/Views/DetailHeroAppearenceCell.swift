//
//  DetailHeroAppearenceCell.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class DetailHeroAppearenceCell: UICollectionViewCell, ReusableView {
  let title = UILabel()
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupSubviews()
  }
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  func setupSubviews() {
    translatesAutoresizingMaskIntoConstraints = false
    title.font = .systemFont(ofSize: 12)
    addSubview(title)
    title.pin(into: self)
  }

  func setup(text: String) {
    title.text = text
  }
}
