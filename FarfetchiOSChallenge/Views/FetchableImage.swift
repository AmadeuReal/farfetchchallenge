//
//  FetchableImage.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class FetchableImage: UIImageView {

  static let cache: NSCache<AnyObject, UIImage> = NSCache()

  var session: RequestDispatcher?

  convenience init(session: RequestDispatcher) {
    self.init()
    self.session = session
  }

  var currentUrl: String? = nil {
    didSet {
      guard let url = currentUrl else {
        return
      }

      fetchFromURL(url)
    }
  }

  func fetchFromURL(_ url: String, placeholder: UIImage? = nil) {
    image = placeholder
    guard let url = URL(string: url) else {
      return
    }
    if let cachedImage = FetchableImage.cache.object(forKey: currentUrl as AnyObject) {
      image = cachedImage
      return
    }

    session?.request(with: URLRequest(url: url)) { [weak self] (data, error) in
      guard let data = data, error == nil else {
        return
      }
      DispatchQueue.main.async { [weak self ] in
        self?.image = UIImage(data: data)
        self?.cacheImage(self?.image, forKey: url.path)
      }
    }

  }

  private func cacheImage(_ image: UIImage?, forKey key: String) {
    if let image = image {
      FetchableImage.cache.setObject(image, forKey: currentUrl as AnyObject)
    }
  }

}
