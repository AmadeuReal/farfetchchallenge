//
//  LoadingView.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class LoadingView: UIView {

  var heightContraint: NSLayoutConstraint!

  lazy var loadingIndicator: UIActivityIndicatorView = {
    let indicator = UIActivityIndicatorView()
    indicator.translatesAutoresizingMaskIntoConstraints = false
    indicator.hidesWhenStopped = true
    return indicator
  }()

  lazy var loadingLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = 0
    label.font = .systemFont(ofSize: 10)
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    translatesAutoresizingMaskIntoConstraints = false

    heightContraint = heightAnchor.constraint(equalToConstant: 0)
    heightContraint.isActive = true

    loadingIndicator.color = .lightGray
    addSubview(loadingIndicator)
    addSubview(loadingLabel)

    NSLayoutConstraint.activate([
      loadingIndicator.heightAnchor.constraint(greaterThanOrEqualTo: heightAnchor, multiplier: 0.5),
      loadingIndicator.widthAnchor.constraint(equalTo: loadingIndicator.heightAnchor),

      loadingIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
      loadingIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),

      loadingLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
      loadingLabel.topAnchor.constraint(equalTo: loadingIndicator.bottomAnchor),
      loadingLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
      ])
  }
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  func startAnimating() {
    isHidden = false
    heightContraint.constant = 80
    loadingIndicator.startAnimating()
    loadingLabel.text = "Loading..."
    UIView.animate(withDuration: 0.1) { [weak self] in
      self?.layoutIfNeeded()
      self?.alpha = 1
    }
  }

  func stopAnimating() {
    heightContraint.constant = 0
    loadingIndicator.stopAnimating()
    loadingLabel.text = nil
    UIView.animate(withDuration: 0.1, animations: { [weak self] in
      self?.layoutIfNeeded()
      self?.alpha = 0
      }, completion: { [weak self] _ in
        self?.isHidden = true
    })
  }
}
