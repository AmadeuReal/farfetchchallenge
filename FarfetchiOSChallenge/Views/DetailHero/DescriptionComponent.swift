//
//  DescriptionComponent.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class DescriptionComponent: UIView {
  lazy var header: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Description"
    label.numberOfLines = 0
    label.font = .boldSystemFont(ofSize: 14)
    return label
  }()

  lazy var detail: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = 0
    label.font = .systemFont(ofSize: 12)
    label.textColor = .lightGray
    return label
  }()

  lazy var stackView: UIStackView = {
    let stack = UIStackView()
    stack.axis = .vertical
    stack.distribution = .fillProportionally
    stack.alignment = .top
    return stack
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    translatesAutoresizingMaskIntoConstraints = false
    setupSubviews()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  func setupSubviews() {
    addSubview(stackView)
    stackView.pin(into: self)

    stackView.addArrangedSubview(header)
    stackView.addArrangedSubview(detail)
  }

  func changeDetailText(to newText: String) {
    detail.text = newText
  }
}
