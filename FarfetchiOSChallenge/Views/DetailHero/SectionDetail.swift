//
//  SectionDetail.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class SectionDetail: UICollectionReusableView, ReusableView {
  lazy var title: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = 0
    label.textColor = .lightGray
    label.font = .boldSystemFont(ofSize: 12)
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .darkGray
    setupSubviews()
  }
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  func setupSubviews() {
    addSubview(title)
    translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      title.bottomAnchor.constraint(equalTo: bottomAnchor),
      title.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor)
      ])
  }
}
