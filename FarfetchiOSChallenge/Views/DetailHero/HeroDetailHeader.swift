//
//  HeroDetailHeader.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class HeroDetailHeader: UIView, ReusableView {
  var heroAvatar = HeroAvatar(session: URLSession.shared)
  var descriptionComponent = DescriptionComponent(frame: .zero)

  lazy var stackView: UIStackView = {
    let stack = UIStackView()
    stack.axis = .horizontal
    stack.distribution = .fillProportionally
    stack.alignment = .center
    return stack
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupSubviews()
  }
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  func setupSubviews() {
    addSubview(stackView)
    stackView.pin(into: self)

    translatesAutoresizingMaskIntoConstraints = false
    heroAvatar.translatesAutoresizingMaskIntoConstraints = false
    descriptionComponent.translatesAutoresizingMaskIntoConstraints = false

    heroAvatar.accessibilityIdentifier = "Hero Avatar"
    descriptionComponent.detail.accessibilityIdentifier = "Hero Information"

    stackView.addArrangedSubview(heroAvatar)
    stackView.addArrangedSubview(descriptionComponent)

    NSLayoutConstraint.activate([
      heroAvatar.widthAnchor.constraint(equalTo: stackView.widthAnchor, multiplier: 0.25),
      heroAvatar.heightAnchor.constraint(equalTo: heroAvatar.widthAnchor),
      descriptionComponent.heightAnchor.constraint(equalTo: stackView.heightAnchor)
      ])
  }

  func setup(for hero: SuperHero) {
    heroAvatar.currentUrl = hero.thumbnail?.fullPath
    if hero.description.isEmpty {
      descriptionComponent.changeDetailText(to: "No description about this character. Do you have more detail about him? Give us your ideas.")
    } else {
      descriptionComponent.changeDetailText(to: hero.description)
    }
  }

}
