//
//  HeroListingController+UICollectionViewDelegate.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

extension HeroListingController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    if shouldFetchMore(for: indexPath) {
      requestHeroes()
    }
  }

  private func shouldFetchMore(for index: IndexPath) -> Bool {
    return index.row >= Int(Double(heroSet.count) * 0.7)
      && heroSet.canFetchMore
  }

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let detailController = HeroDetailController(hero: heroSet.heroes[indexPath.row])
    navigationController?.show(detailController, sender: nil)
  }
}
