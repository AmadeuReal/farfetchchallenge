//
//  HeroListingController+UICollectionViewDataSource.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

extension HeroListingController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return heroSet.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueCell(class: HeroCell.self, for: indexPath),
      indexPath.row < heroSet.count else {
        return UICollectionViewCell()
    }
    cell.setup(with: heroSet.heroes[indexPath.row])
    return cell
  }
}
