//
//  CustomAnimator.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 04/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class CustomAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  var duration: TimeInterval
  var isPresenting: Bool

  init(duration: TimeInterval, isPresenting: Bool) {
    self.duration = duration
    self.isPresenting = isPresenting
  }

  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    let container = transitionContext.containerView

    guard let fromView = transitionContext.view(forKey: .from) else { return }
    guard let toView = transitionContext.view(forKey: .to) else { return }

    self.isPresenting ? container.addSubview(toView) : container.insertSubview(toView, belowSubview: fromView)

    let detailView = isPresenting ? toView : fromView

    toView.frame = isPresenting ?  CGRect(x: 0, y: fromView.frame.height, width: toView.frame.width, height: toView.frame.height) : toView.frame
    toView.alpha = isPresenting ? 0 : 1
    toView.layoutIfNeeded()

    UIView.animate(withDuration: duration, animations: {
      detailView.frame = self.isPresenting ? fromView.frame : CGRect(x: 0, y: fromView.frame.height, width: toView.frame.width, height: toView.frame.height)
      detailView.alpha = self.isPresenting ? 1 : 0
    }, completion: { (_) in
      transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    })
  }

  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return duration
  }
}
