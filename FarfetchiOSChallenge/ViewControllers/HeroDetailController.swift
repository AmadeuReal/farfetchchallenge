//
//  HeroDetailController.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class HeroDetailController: UIViewController {

  enum HeroInfo: String, CaseIterable {
    case events, comics, stories, series
  }

  var descriptionComponent = HeroDetailHeader(frame: .zero)

  lazy var collectionView: UICollectionView = {
    let collection = UICollectionView(frame: .zero, collectionViewLayout: DetailFlowLayout())
    collection.dataSource = self
    collection.register(SectionDetail.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SectionDetail.identifier)
    collection.register(DetailHeroAppearenceCell.self)
    collection.backgroundColor = .clear
    collection.translatesAutoresizingMaskIntoConstraints = false
    return collection
  }()

  var hero: SuperHero!
  convenience init(hero: SuperHero) {
    self.init(nibName: nil, bundle: nil)
    self.hero = hero
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    if hero == nil {
      dismiss(animated: true, completion: nil)
      return
    }

    title = hero.name
    view.backgroundColor = .white

    view.addSubview(collectionView)
    view.addSubview(descriptionComponent)
    descriptionComponent.setup(for: hero)
    let margin = view.layoutMarginsGuide
    NSLayoutConstraint.activate([
      descriptionComponent.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      descriptionComponent.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      descriptionComponent.topAnchor.constraint(equalTo: margin.topAnchor),

      collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      collectionView.topAnchor.constraint(equalTo: descriptionComponent.bottomAnchor),
      collectionView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
      ])
  }

  func getAppearenceAt(section: Int) -> Appearance? {
    switch HeroInfo.allCases[section] {
    case .comics: return hero.comics
    case .series: return hero.series
    case .stories: return hero.stories
    case .events: return hero.events
    }
  }
}

extension HeroDetailController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return max(min(getAppearenceAt(section: section)?.items.count ?? 0, 3), 1)
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueCell(class: DetailHeroAppearenceCell.self, for: indexPath) else {
      return UICollectionViewCell()
    }
    if let appearance = getAppearenceAt(section: indexPath.section),
      appearance.items.count > indexPath.row {
      cell.setup(text: appearance.items[indexPath.row].name)
    } else {
      cell.setup(text: "No \(HeroInfo.allCases[indexPath.section].rawValue) recorded")
    }

    return cell
  }

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return HeroInfo.allCases.count
  }

  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    guard let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SectionDetail.identifier, for: indexPath) as? SectionDetail else {
      return UICollectionReusableView()
    }
    view.title.text = HeroInfo.allCases[indexPath.section].rawValue.capitalized
    return view
  }
}
