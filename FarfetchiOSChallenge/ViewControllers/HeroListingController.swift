//
//  HeroListingController.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit

class HeroListingController: UIViewController {

  /// In order to control the rate of network requests for filtering super heroes by name
  /// this variable will "hold" the request for 1s. And only after that period of time
  /// and if no other call to its `throttle` method is that the request will be made.
  var searchThrottler = Throttler(seconds: 1)

  lazy var loadingIndicator: LoadingView = LoadingView()
  lazy var collectionView: UICollectionView = {
    let collection = UICollectionView(frame: .zero, collectionViewLayout: HeroListFlowLayout())
    collection.register(HeroCell.self)
    collection.delegate = self
    collection.dataSource = self
    collection.backgroundColor = .clear
    collection.translatesAutoresizingMaskIntoConstraints = false
    return collection
  }()
  lazy var searchController: UISearchController = {
    let search = UISearchController(searchResultsController: nil)
    search.searchResultsUpdater = self
    search.delegate = self
    search.hidesNavigationBarDuringPresentation = false
    search.dimsBackgroundDuringPresentation = false
    search.searchBar.placeholder = "Search for your favorite heroes"
    return search
  }()
  private lazy var warningLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 12)
    label.textColor = .lightGray
    label.numberOfLines = 0
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  var provider = SuperHeroProvider()

  var filteringByName: String = "" {
    willSet {
      title = newValue.isEmpty ? "Marvel Heroes" : "Looking for \(newValue)"
    }
  }

  var heroSet: HeroesSet = HeroesSet.empty
  var isFetching: Bool = false {
    willSet(value) {
      DispatchQueue.main.async { [weak self] in
        UIApplication.shared.isNetworkActivityIndicatorVisible = value
        value ?
          self?.loadingIndicator.startAnimating() :
          self?.loadingIndicator.stopAnimating()
      }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white

    navigationController?.delegate = self
    navigationController?.navigationBar.prefersLargeTitles = true
    navigationItem.titleView = searchController.searchBar

    view.addSubview(collectionView)
    view.addSubview(loadingIndicator)
    view.addSubview(warningLabel)
    setupLayoutConstraint()

    filteringByName = ""
    requestHeroes()

    tabBarItem = UITabBarItem(tabBarSystemItem: .mostViewed, tag: 1)
    tabBarItem.title = "Heroes"

  }

  private func setupLayoutConstraint() {
    let margin = view.layoutMarginsGuide
    NSLayoutConstraint.activate([
      loadingIndicator.topAnchor.constraint(equalTo: margin.topAnchor),
      loadingIndicator.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      loadingIndicator.trailingAnchor.constraint(equalTo: view.trailingAnchor),

      collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      collectionView.topAnchor.constraint(equalTo: loadingIndicator.bottomAnchor),
      collectionView.bottomAnchor.constraint(equalTo: margin.bottomAnchor),

      warningLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      warningLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      warningLabel.widthAnchor.constraint(lessThanOrEqualTo: view.layoutMarginsGuide.widthAnchor)
      ])
  }

  func gotHeroesFromRequest(data: MarvelData<[SuperHero]>) {
    if heroSet.isStartPage {
      heroSet.heroes = data.results
    } else {
      heroSet.heroes.append(contentsOf: data.results)
    }
    heroSet.canFetchMore = data.canFetchForMoreData
    heroSet.currentPage += 1
    DispatchQueue.main.async { [weak self] in
      self?.collectionView.reloadData()
      if self?.heroSet.count == 0 {
        self?.show(message: "Sorry! We couldn't find any super heroe that matches your query. :'(")
      } else {
        self?.hideWarningLabel()
      }
      self?.isFetching = false
    }
  }

  func show(message: String) {
    warningLabel.text = message
    UIView.animate(withDuration: 0.2) { [weak self] in
      self?.warningLabel.alpha = 1
    }
  }

  func hideWarningLabel() {
    warningLabel.text = nil
    UIView.animate(withDuration: 0.2) { [weak self] in
      self?.warningLabel.alpha = 0
    }
  }

  func requestHeroes() {
    guard !isFetching else {
      return
    }
    isFetching = true
    provider.requestHeroes(name: filteringByName, page: heroSet.currentPage) { [weak self] data in
      guard let data = data else {
        self?.isFetching = false
        return
      }
      self?.gotHeroesFromRequest(data: data)
    }
  }
}

extension HeroListingController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    guard let name = searchController.searchBar.text else {
      return
    }
    filteringByName = name
    searchThrottler.throttle { [weak self] in
      self?.heroSet.currentPage = 0
      self?.requestHeroes()
    }
  }
}
extension HeroListingController: UISearchControllerDelegate {
  func willDismissSearchController(_ searchController: UISearchController) {
    if searchController.searchBar.text?.isEmpty == false {
      filteringByName = ""
      heroSet.currentPage = 0
      requestHeroes()
    }
  }
}

extension HeroListingController: UINavigationControllerDelegate {
  func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    switch operation {
    case .push:
      return CustomAnimator(duration: TimeInterval(UINavigationController.hideShowBarDuration), isPresenting: true)
    default:
      return CustomAnimator(duration: TimeInterval(UINavigationController.hideShowBarDuration), isPresenting: false)
    }
  }
}
