//
//  Reusable.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import UIKit.UIView

protocol ReusableView {
  static var identifier: String { get }
}
extension ReusableView where Self: UIView {
  static var identifier: String {
    return String(describing: Self.self)
  }
}
