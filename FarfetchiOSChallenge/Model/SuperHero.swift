//
//  SuperHero.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

struct SuperHero: MarvelCharacter {
  var name: String
  var description: String
  var thumbnail: Thumbnail?

  var comics: Appearance?
  var events: Appearance?
  var series: Appearance?
  var stories: Appearance?
}
