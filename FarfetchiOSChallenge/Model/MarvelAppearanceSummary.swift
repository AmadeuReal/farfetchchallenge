//
//  MarvelAppearanceSummary.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

struct MarvelAppearanceSummary: Decodable {
  /// The name of the event.
  var name: String
}
