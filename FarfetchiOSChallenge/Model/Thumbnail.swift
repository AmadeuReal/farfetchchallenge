//
//  Thumbnail.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

struct Thumbnail: MarvelThumbnail {
  var path: String
  var `extension`: String
}
