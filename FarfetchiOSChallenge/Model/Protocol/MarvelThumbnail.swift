//
//  MarvelThumbnail.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

protocol MarvelThumbnail: Decodable {

  ///  The directory path of to the image.
  var path: String { get }

  /// The file extension for the image.
  var `extension`: String { get }

  /// Combination of the element path and extension
  var fullPath: String { get }
}

extension MarvelThumbnail {
  var fullPath: String {
    return "\(path).\(`extension`)"
  }
}
