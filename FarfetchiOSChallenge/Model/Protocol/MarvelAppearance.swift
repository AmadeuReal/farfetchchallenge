//
//  MarvelAppearance.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

protocol MarvelAppearance: Decodable {

  /// The list of returned events in a certain collection.
  var items: [MarvelAppearanceSummary] { get }
}
