//
//  MarvelCharacter.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

protocol MarvelCharacter: Decodable {

  /// The name of the character.
  var name: String { get }

  ///  A short bio or description of the character.
  var description: String { get }
}
