//
//  Throttler.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

public class Throttler {

  typealias Action = () -> Void
  private let queue: DispatchQueue = DispatchQueue.global(qos: .background)

  private var job: DispatchWorkItem?
  private var previousRun: Date = Date.distantPast
  private var maxInterval: Double

  init(seconds: Double) {
    self.maxInterval = seconds
  }

  func throttle(block: @escaping Action) {
    guard var job = job else {
      let job = getJob(for: block)
      queue.asyncAfter(deadline: .now() + maxInterval, execute: job)
      self.job = job
      return
    }
    cancel()
    job = getJob(for: block)
    let delay = Date().seconds(from: previousRun) > maxInterval ? 0 : maxInterval
    queue.asyncAfter(deadline: .now() + delay, execute: job)
    self.job = job
  }

  func cancel() {
    job?.cancel()
    job = nil
  }

  private func getJob(for block: @escaping Action) -> DispatchWorkItem {
    return DispatchWorkItem { [weak self] in
      self?.previousRun = Date()
      block()
    }
  }

  deinit {
    cancel()
  }
}
