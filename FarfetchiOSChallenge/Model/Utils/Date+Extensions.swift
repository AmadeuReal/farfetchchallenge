//
//  Date+Extensions.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

extension Date {
  func seconds(from date: Date) -> Double {
    return timeIntervalSince(date).rounded()
  }
}
