//
//  Secrets.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

struct Secrets {
  static var privateKey: String {
    return Bundle.main.infoDictionary?["MARVEL_PRIVATE_KEY"] as? String ?? ""
  }
  static var publicKey: String {
    return Bundle.main.infoDictionary?["MARVEL_PUBLIC_KEY"] as? String ?? ""
  }
}
