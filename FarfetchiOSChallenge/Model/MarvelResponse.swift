//
//  MarvelResponse.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

struct MarvelResponse<T: Decodable>: Decodable {
  var code: Int
  var status: String
  var data: MarvelData<T>
}

struct MarvelData<T: Decodable>: Decodable {
  typealias Element = T
  var offset: Int
  var limit: Int
  var total: Int
  var count: Int
  var results: Element

  var canFetchForMoreData: Bool {
    return offset + limit < total
  }
}
