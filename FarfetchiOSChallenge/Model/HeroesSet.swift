//
//  HeroesSet.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 02/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

struct HeroesSet {
  var heroes: [SuperHero]
  var currentPage: Int
  var canFetchMore: Bool
  static let empty = HeroesSet(heroes: [], currentPage: 0, canFetchMore: true)

  var isStartPage: Bool {
    return currentPage == 0
  }
  var count: Int {
    return heroes.count
  }
}
