//
//  SuperHeroProvider.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

struct SuperHeroProvider {
  let provider: Network
  init(provider: Network = NetworkProvider()) {
    self.provider = provider
  }

  func requestHeroes(name: String, page: Int, _ completion: @escaping ((MarvelData<[SuperHero]>?) -> Void)) {
    provider.request(.superHero(name: name, page: page)) { (response: NetworkResponse<[SuperHero]>) in
      switch response {
      case .sucess(let resource): completion(resource.data)
      case .fail: completion(nil)
      }
    }
  }
}
