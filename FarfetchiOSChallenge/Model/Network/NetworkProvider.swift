//
//  NetworkProvider.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

struct NetworkProvider: Network {
  let dispatcher: RequestDispatcher
  init(_ dispatcher: RequestDispatcher = URLSession.shared) {
    self.dispatcher = dispatcher
  }
  func request<T: Decodable>(_ target: Target, _ completion: @escaping ((NetworkResponse<T>) -> Void)) {
    guard let url = URL(string: "\(Target.baseUrl)\(target.path)") else {
      completion(.fail(NetworkError.invalidUrl))
      return
    }

    var urlRequest = URLRequest(url: url)
    urlRequest.httpMethod = target.method.name
    target.parameterEncoder?.encode(request: &urlRequest, with: target.params)

    dispatcher.request(with: urlRequest) { (data, error) in
      if let error = error {
        completion(.fail(error))
        return
      }
      guard let data = data else {
        completion(.fail(NetworkError.noData))
        return
      }

      do {
        let decodeData = try JSONDecoder().decode(MarvelResponse<T>.self, from: data)
        completion(.sucess(decodeData))
      } catch let error {
        completion(.fail(error))
      }

    }
  }
}
public protocol ParameterEncoder {
  func encode(request: inout URLRequest, with parameters: [String: Any])
}

struct URLParameterEncoder: ParameterEncoder {
  func encode(request: inout URLRequest, with parameters: [String: Any]) {
    guard let url = request.url else {
      return
    }

    if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) {
      urlComponents.queryItems = parameters.map {
        return URLQueryItem(name: $0.key,
                            value: "\($0.value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
      }
      request.url = urlComponents.url
    }

    request.addValue("application/www-form-urlencoded; charset=utf8", forHTTPHeaderField: "Content-Type")
  }

}
