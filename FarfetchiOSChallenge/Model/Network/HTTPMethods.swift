//
//  HTTPMethods.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

enum HTTPMethods: String {
  case get

  var name: String {
    return rawValue.uppercased()
  }
}
