//
//  Network.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

enum NetworkResponse<T: Decodable> {
  case sucess(MarvelResponse<T>), fail(Error)
}

enum NetworkError: Error {
  case noData, invalidUrl
}

protocol Network {
  func request<T: Decodable>(_ target: Target, _ completion: @escaping ((NetworkResponse<T>) -> Void))
}
