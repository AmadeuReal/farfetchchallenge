//
//  NetworkRequest.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

protocol TargetType {
  static var baseUrl: String { get }
  var path: String { get }
  var method: HTTPMethods { get }
  var params: [String: Any] { get }
  var parameterEncoder: ParameterEncoder? { get }
}

enum Target: TargetType {
  static var baseUrl: String = "https://gateway.marvel.com:443/v1/public"

  case superHero(name: String, page: Int)

  var path: String {
    return "/characters"
  }

  var method: HTTPMethods {
    return .get
  }

  var params: [String: Any] {
    var parameters: [String: Any] = [
      "ts": Int(Date().timeIntervalSince1970),
      "apikey": Secrets.publicKey,
      "hash": "\(Int(Date().timeIntervalSince1970))\(Secrets.privateKey)\(Secrets.publicKey)".md5,
      "limit": 20
    ]
    if case .superHero(let name, let page) = self {
      parameters["offset"] = page * 20
      if !name.isEmpty { parameters["nameStartsWith"] = name }
    }
    return parameters
  }

  var parameterEncoder: ParameterEncoder? {
    return URLParameterEncoder()
  }
}
