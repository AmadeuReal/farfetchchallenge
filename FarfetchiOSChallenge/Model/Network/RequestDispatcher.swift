//
//  RequestDispatcher.swift
//  FarfetchiOSChallenge
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation

protocol RequestDispatcher {
  func request(with request: URLRequest, completionHandler: @escaping (Data?, Error?) -> Void)
}
extension URLSession: RequestDispatcher {
  func request(with request: URLRequest, completionHandler: @escaping (Data?, Error?) -> Void) {
    dataTask(with: request) { (data, _, error) in
      completionHandler(data, error)
      }.resume()
  }
}
