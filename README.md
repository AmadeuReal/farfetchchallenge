# Farfetch iOS Challenge

## Detail

The purpose of this project is to make a techinal assessment for the an iOS engineer position at Farfetch.

## Project description

It is an application that uses Marvel's API in order to display and search super heroes from their Universe.

## Warning

In order to fetch data from Marvel's API, you have to have two files `.marvel_pub` and `.marvel_priv`, in which you will have to store your, respectively, public key and private key.

```
  echo $MARVEL_PUBLIC_KEY > .marvel_pub
  echo $MARVEL_PRIVATE_KEY > .marvel_priv
```

After that you are good to go.

## Requirements

- Xcode 10
- Swift 4.2
- iOS 12
