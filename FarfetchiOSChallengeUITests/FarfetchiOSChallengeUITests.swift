//
//  FarfetchiOSChallengeUITests.swift
//  FarfetchiOSChallengeUITests
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import XCTest

class FarfetchiOSChallengeUITests: XCTestCase {

  let app = XCUIApplication()

  override func setUp() {
    super.setUp()

    XCUIDevice.shared.orientation = .portrait
    continueAfterFailure = false
    app.launch()
  }

  func testFailedQuery() {
    let marvelHeroesNavigationBar = app.navigationBars["Marvel Heroes"]
    marvelHeroesNavigationBar.otherElements["Marvel Heroes"].tap()
    let searchField = marvelHeroesNavigationBar.searchFields["Search for your favorite heroes"]

    searchField.tap()
    searchField.typeText("Random!")

    let fetchingForHeroNav = app.navigationBars["Looking for Random!"]
    XCTAssert(fetchingForHeroNav.exists, "There should be an indicator showing that a query was made.")

    let warning = app.staticTexts["Sorry! We couldn't find any super heroe that matches your query. :'("]

    let existsPredicate = NSPredicate(format: "exists == true")
    expectation(for: existsPredicate, evaluatedWith: warning, handler: nil)

    waitForExpectations(timeout: 3, handler: nil)
    XCTAssert(warning.exists, "There should be a message saying that query failed.")
  }

  func testSearchByName() {

    let marvelHeroesNavigationBar = app.navigationBars["Marvel Heroes"]
    marvelHeroesNavigationBar.otherElements["Marvel Heroes"].tap()
    let searchField = marvelHeroesNavigationBar.searchFields["Search for your favorite heroes"]

    searchField.tap()
    searchField.typeText("Amadeu")

    let cell = app.collectionViews.children(matching: .cell).firstMatch
    let cellHeader = cell/*@START_MENU_TOKEN@*/.staticTexts["Amadeus Cho"]/*[[".cells.staticTexts[\"Amadeus Cho\"]",".staticTexts[\"Amadeus Cho\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/

    let existsPredicate = NSPredicate(format: "exists == true")
    expectation(for: existsPredicate, evaluatedWith: cellHeader, handler: nil)

    waitForExpectations(timeout: 3, handler: nil)
    XCTAssert(cellHeader.exists, "There should be a hero called Amadeus Cho that matches 'Amadeu' filter.")

    cell.tap()
    goToDetail()
  }

  func goToDetail() {

    app.staticTexts["Description"].tap()
    app/*@START_MENU_TOKEN@*/.staticTexts["Hero Information"]/*[[".staticTexts[\"No description about this character. Do you have more detail about him? Give us your ideas.\"]",".staticTexts[\"Hero Information\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

    let collectionViewsQuery = app.collectionViews
    collectionViewsQuery.staticTexts["Events"].tap()
    collectionViewsQuery.staticTexts["Comics"].tap()
    collectionViewsQuery.staticTexts["Stories"].tap()
    collectionViewsQuery.staticTexts["Series"].tap()
  }
}
