//
//  FarfetchiOSChallengeTests.swift
//  FarfetchiOSChallengeTests
//
//  Created by Amadeu Martos on 29/11/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import XCTest
@testable import FarfetchiOSChallenge

class SuperHeroSpec: XCTestCase {

  func testInitialization() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":""
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertEqual(hero?.name, "3-D Man")
    XCTAssertEqual(hero?.description.isEmpty, true, "Should have no description")

  }

  func testSuperHeroWithThumbnail() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "thumbnail":{
          "path":"http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
          "extension":"jpg"
          }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertEqual(hero?.name, "3-D Man")
    XCTAssertEqual(hero?.description.isEmpty, true, "Should have no description")
    XCTAssertEqual(hero?.thumbnail?.path, "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784")
    XCTAssertEqual(hero?.thumbnail?.fullPath, "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")
  }

  func testSuperHeroWithEmptyComics() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "comics": { "items": [] }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertNotNil(hero?.comics, "There should be a variable holding comics")
    XCTAssertEqual(hero?.comics?.items.isEmpty, true)
  }

  func testSuperHeroWithComicsEnvolved() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "comics": {
          "items": [ {
          "name": "Random Comic #1"
          }]
        }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertNotNil(hero?.comics, "There should be a variable holding comics")
    XCTAssertEqual(hero?.comics?.items.isEmpty, false)
  }

  func testSuperHeroWithEmptyEvents() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "comics": { "items": [] },
        "events": { "items": [] }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertNotNil(hero?.events, "There should be a variable holding comics")
    XCTAssertEqual(hero?.events?.items.isEmpty, true)
  }

  func testSuperHeroWithEventsEnvolved() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "comics": { "items": [] },
        "events": { "items": [
          {"name": "Random Events #1"}
        ] }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertNotNil(hero?.events, "There should be a variable holding comics")
    XCTAssertEqual(hero?.events?.items.isEmpty, false)
  }

  func testSuperHeroWithEmptySeries() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "comics": { "items": [] },
        "events": { "items": [] },
        "series": { "items": [] }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertNotNil(hero?.series, "There should be a variable holding series")
    XCTAssertEqual(hero?.series?.items.isEmpty, true)
  }

  func testSuperHeroWithSeriesEnvolved() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "comics": { "items": [] },
        "events": { "items": [] },
        "series": {
          "items": [ {
          "name": "Random Serie #1"
          }]
        }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertNotNil(hero?.series, "There should be a variable holding series")
    XCTAssertEqual(hero?.series?.items.isEmpty, false)
  }

  func testSuperHeroWithEmptyStories() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "comics": { "items": [] },
        "events": { "items": [] },
        "series": { "items": [] },
        "stories": { "items": [] }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertNotNil(hero?.stories, "There should be a variable holding stories")
    XCTAssertEqual(hero?.stories?.items.isEmpty, true)
  }

  func testSuperHeroWithStoriesEnvolved() {
    let json = """
        {
        "id":1011334,
        "name":"3-D Man",
        "description":"",
        "modified":"2014-04-29T14:18:17-0400",
        "comics": { "items": [] },
        "events": { "items": [] },
        "series": { "items": [] },
        "stories": { "items": [
          {"name": "Random Story #1"}
        ] }
        }
    """.data(using: .utf8)!

    let hero = try? JSONDecoder().decode(SuperHero.self, from: json)
    XCTAssertNotNil(hero, "Super Man should be initialized")
    XCTAssertNotNil(hero?.stories, "There should be a variable holding stories")
    XCTAssertEqual(hero?.stories?.items.isEmpty, false)
  }
}
