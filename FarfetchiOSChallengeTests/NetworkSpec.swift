//
//  NetworkSpec.swift
//  FarfetchiOSChallengeTests
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import XCTest
@testable import FarfetchiOSChallenge

class NetworkSpec: XCTestCase {
  func testPossibleResponses() {
    let expectation = XCTestExpectation(description: "Data returned from network should conform with SuperHero model")
    let networkProvider = NetworkProvider(NoDataDispatcher())
    networkProvider.request(.superHero(name: "", page: 0)) { (response: NetworkResponse<[SuperHero]>) in
      switch response {
      case .sucess: expectation.fulfill()
      case .fail: expectation.fulfill()
      }
    }
    wait(for: [expectation], timeout: 1)
  }
  func testWhenReceivingError() {
    let expectation = XCTestExpectation(description: "handle error message")
    let networkProvider = NetworkProvider(ErrorDispatcher())
    networkProvider.request(.superHero(name: "", page: 0)) { (response: NetworkResponse<[SuperHero]>) in
      if case .fail = response {
        expectation.fulfill()
      }
    }
    wait(for: [expectation], timeout: 1)
  }
  func testReceivingValidData() {
    let expectation = XCTestExpectation(description: "It should properly create the list of Heroes")
    let networkProvider = NetworkProvider(DataDispatcher())
    networkProvider.request(.superHero(name: "", page: 0)) { (response: NetworkResponse<[SuperHero]>) in
      if case .sucess(let heroes) = response, !heroes.data.results.isEmpty {
        expectation.fulfill()
      }
    }
    wait(for: [expectation], timeout: 1)
  }
  func testInvalidJson() {
    let expectation = XCTestExpectation(description: "It should gracefully fail when receiving an invalid json")
    let networkProvider = NetworkProvider(InvalidDataDispatcher())
    networkProvider.request(.superHero(name: "", page: 0)) { (response: NetworkResponse<[SuperHero]>) in
      if case .fail = response {
        expectation.fulfill()
      }
    }
    wait(for: [expectation], timeout: 1)
  }

  func testRequestSuperHero() {
    let expectation = XCTestExpectation(description: "Super Hero provider should return properly heroes")
    let heroesProvider = SuperHeroProvider(provider: NetworkProvider(DataDispatcher()))
    heroesProvider.requestHeroes(name: "", page: 0) { movies in
      if !movies!.results.isEmpty {
        expectation.fulfill()
      }
    }
    wait(for: [expectation], timeout: 1)
  }

  func testPageIncrement() {
    let pageZeroExpectation = XCTestExpectation(description: "URL should increment the offset")
    let pageOneExpectation = XCTestExpectation(description: "URL should increment the offset")
    let pageTwoExpectation = XCTestExpectation(description: "URL should increment the offset")

    let provider: (Int, XCTestExpectation) -> DispatchListeningForQuery = { (limit, expectation) in
      return DispatchListeningForQuery(key: "offset", onRequest: { offset in
        if let value = Int(offset), value == limit {
          expectation.fulfill()
        }
      })
    }

    SuperHeroProvider(provider: NetworkProvider(provider(0, pageZeroExpectation))).requestHeroes(name: "", page: 0) { _ in }
    SuperHeroProvider(provider: NetworkProvider(provider(20, pageOneExpectation))).requestHeroes(name: "", page: 1) { _ in }
    SuperHeroProvider(provider: NetworkProvider(provider(40, pageTwoExpectation))).requestHeroes(name: "", page: 2) { _ in }

    wait(for: [pageZeroExpectation, pageOneExpectation, pageTwoExpectation], timeout: 1)
  }

  func testFetchByName() {
    let expectation = XCTestExpectation(description: "URL should increment the offset")
    let provider = DispatchListeningForQuery(key: "nameStartsWith") { response in
      if response == "Spi" {
        expectation.fulfill()
      }
    }

    SuperHeroProvider(provider: NetworkProvider(provider)).requestHeroes(name: "Spi", page: 0) { _ in }
    wait(for: [expectation], timeout: 1)
  }
}
