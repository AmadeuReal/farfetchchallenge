//
//  Dispatchers.swift
//  FarfetchiOSChallengeTests
//
//  Created by Amadeu Martos on 01/12/2018.
//  Copyright © 2018 Amadeu Real. All rights reserved.
//

import Foundation
@testable import FarfetchiOSChallenge

struct NoDataDispatcher: RequestDispatcher {
  func request(with request: URLRequest, completionHandler: @escaping (Data?, Error?) -> Void) {
    completionHandler(nil, nil)
  }
}
struct ErrorDispatcher: RequestDispatcher {
  func request(with request: URLRequest, completionHandler: @escaping (Data?, Error?) -> Void) {
    completionHandler(nil, NSError(domain: "com.amadeu.farfetch", code: 1000, userInfo: nil))
  }
}
struct DataDispatcher: RequestDispatcher {
  func request(with request: URLRequest, completionHandler: @escaping (Data?, Error?) -> Void) {
    guard let path = Bundle.main.path(forResource: "ListHero", ofType: "json") else {
      completionHandler(nil, NetworkError.noData)
      return
    }
    do {
      let data = try Data(contentsOf: URL(fileURLWithPath: path))
      completionHandler(data, nil)
    } catch let error {
      completionHandler(nil, error)
    }
  }
}
struct InvalidDataDispatcher: RequestDispatcher {
  func request(with request: URLRequest, completionHandler: @escaping (Data?, Error?) -> Void) {
    completionHandler("{".data(using: .utf8), nil)
  }
}
struct DispatchListeningForQuery: RequestDispatcher {
  var key: String
  var onRequest: (String) -> Void
  func request(with request: URLRequest, completionHandler: @escaping (Data?, Error?) -> Void) {
    let offsetValue = request.url?.query?.split(separator: "&")
      .filter { $0.contains(key) }
      .compactMap { $0.split(separator: "=").last }
      .first
    if let value = offsetValue {
      onRequest(String(value))
    }
  }
}
